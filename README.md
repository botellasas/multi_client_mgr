Crear y mantener multiples instancias de odoo
---------------------------------------------
Para agregar un cliente nuevo hay que actualizar el config.yaml
que es un archivo con sintaxis yaml este es un ejemplo:

    polimera:
      version: 11.0
      port: 80
      type: EE
    piscinas:
      version: 11.0
      port: 81
      type: CE

| comando  | descripcion |
| ------------- | ------------- |
| oe -i -c cliente  | instalar o actualizar la instancias de este cliente |
| oe -w -c cliente  | escribir odoo.conf de este cliente |
| sd rmall | matar todos los contenedores |
| ./run_env.sh | arrancar los contenedores de postgres y aeroo comunes a todas las instancias |
| ./multi.py -r -c cliente | arrancar el cliente de nombre cliente |
| ./multi.py -u -m all -c cliente | actualizar base de datos del cliente (baja el cliente actualiza la bd y lo vuelve a levantar) |
| ./multi.py -u -m modulo -c cliente | actualizar solo el modulo modulo del cliente cliente, (baje el cliente actualiza y lo vuelve a levantar) |
| ./multi.py -s -c cliente | bajar un cliente |
| ./multi.py -s -r -c cliente | reinicia un cliente |
| sd rmall | baja todas las imagenes todo |
| ./restore_database.sh | restaura un backup de produccion en test Parametros: cliente 11.0 (para CE) 11.0e (para EE) |
