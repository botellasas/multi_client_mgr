# arrancar imagen con debuger
sudo docker run -d \
	-p 1984:1984 \
	--name=wdb kozea/wdb:3.2.5

# arrancar imagen de polimera apuntando con link a debuger, postgres y aeroo
sudo docker run --rm -it \
	--link aeroo:aeroo \
	--link wdb \
	-p 80:8069 \
	-v /odoo_ar/odoo-11.0e/polimera/config:/opt/odoo/etc/ \
	-v /odoo_ar/odoo-11.0e/polimera/data_dir:/opt/odoo/data \
	-v /odoo_ar/odoo-11.0e/polimera/log:/var/log/odoo \
	-v /odoo_ar/odoo-11.0e/polimera/sources:/opt/odoo/custom-addons \
	-v /odoo_ar/odoo-11.0e/polimera/backup_dir:/var/odoo/backups/ \
	-v /odoo_ar/odoo-11.0e/extra-addons:/opt/odoo/extra-addons \
	-v /odoo_ar/odoo-11.0e/dist-packages:/usr/lib/python3/dist-packages \
	--link postgres:db \
	--name polimera_debug \
	-e ODOO_CONF=/dev/null \
	-e SERVER_MODE=test \
	-e WDB_SOCKET_SERVER=wdb \
	jobiols/odoo-ent:11.0e.debug \
	--logfile=/dev/stdout

# terminar la imagen de debuger
sd rm -f wdb
