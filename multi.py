#!/usr/bin/env python3

import argparse
import subprocess
import yaml

import sys

RED = "\033[1;31m"
GREEN = "\033[1;32m"
YELLOW = "\033[1;33m"
YELLOW_LIGHT = "\033[33m"
CLEAR = "\033[0;m"


class Msg():
    @staticmethod
    def green(string):
        return GREEN + string + CLEAR

    @staticmethod
    def yellow(string):
        return YELLOW + string + CLEAR

    @staticmethod
    def red(string):
        return RED + string + CLEAR

    @staticmethod
    def yellow_light(string):
        return YELLOW_LIGHT + string + CLEAR

    def run(self, msg):
        print (self.yellow(msg))

    def done(self, msg):
        print (self.green(msg))

    def err(self, msg):
        print (self.red(msg))
        sys.exit()

    def inf(self, msg):
        if msg:
            print (self.yellow_light(msg))

    def warn(self, msg):
        print (self.red(msg))


def get_config():
    try:
        with open('config.yaml', 'r') as stream:
            config = yaml.load(stream, Loader=yaml.FullLoader)
    except Exception as ex:
        Msg().err('Error {} {}'.format(ex.context, ex.context_mark))
    return config


def call(command, verbose):
    if verbose:
        Msg().inf(command)
    subprocess.call(command, shell=True)


def run_client(clientname, verbose):
    config = get_config()
    client = config[clientname]
    port = client['port']
    version = client['version']
    type = client['type']
    ver = 'e' if type == 'EE' else ''
    img = 'ent' if type == 'EE' else 'ce'

    command = 'sudo docker run -d '
    command += '--link aeroo:aeroo '
    command += '-p {}:8069 '.format(port)
    dir = '/odoo_ar/odoo-{}{}/{}'.format(version, ver, clientname)
    command += '-v {}/config:/opt/odoo/etc/ '.format(dir)
    command += '-v {}/data_dir:/opt/odoo/data '.format(dir)
    command += '-v {}/log:/var/log/odoo '.format(dir)
    command += '-v {}/sources:/opt/odoo/custom-addons '.format(dir)
    command += '-v {}/backup_dir:/var/odoo/backups/ '.format(dir)
    command += '--link pg-botella:db '
    command += '--restart=always '
    command += '--name {} '.format(clientname)
    command += '-e ODOO_CONF=/dev/null '
    command += '-e SERVER_MODE= '
    command += 'regaby/odoo-{}:{}{} '.format(img, version, ver)
    command += '--logfile=/var/log/odoo/odoo.log '
    command += '--db-filter=^{}_ '.format(clientname)
    call(command, verbose)


def stop_client(clientname, verbose):
    command = 'sudo docker rm -f {}'.format(clientname)
    call(command, verbose)


def update_all(clientname, module, verbose):
    config = get_config()
    client = config[clientname]
    version = client['version']
    type = client['type']
    ver = 'e' if type == 'EE' else ''
    img = 'ent' if type == 'EE' else 'ce'

    dir = '/odoo_ar/odoo-{}{}/{}'.format(version, ver, clientname)
    command = 'sudo docker run --rm -it '
    command += '-v {}/config:/opt/odoo/etc/ '.format(dir)
    command += '-v {}/data_dir:/opt/odoo/data '.format(dir)
    command += '-v {}/log:/var/log/odoo '.format(dir)
    command += '-v {}/sources:/opt/odoo/custom-addons '.format(dir)
    command += '-v {}/backup_dir:/var/odoo/backups/ '.format(dir)
    command += '--link pg-botella:db '
    command += '-e ODOO_CONF=/dev/null '
    command += 'regaby/odoo-{}:{}{} '.format(img, version, ver)
    command += '--stop-after-init '
    command += '--logfile=false '
    command += '-d {}_prod -u {} '.format(clientname, module[0])
    call(command, verbose)


def get_param(args, param):
    if param == 'client':
        if args.client:
            return args.client[0]
        else:
            Msg().err('Need -c option (client name). Process aborted')

    if param == 'database':
        if args.database:
            return args.database[0]
        else:
            Msg().err('Need -d option (database name). Process aborted')

    if param == 'module':
        if args.module:
            return args.module
        else:
            Msg().err('Need -m option (module(s) name). Process aborted')

    if param == 'backup_file':
        if args.backup_file:
            return args.backup_file[0]
        else:
            Msg().err('Need -f option (backup file). Process aborted')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="""
==========================================================================
Odoo Multi Env Manager v0.0.1 - by jeo Software <jorge.obiols@gmail.com>
==========================================================================
""")

    parser.add_argument(
        '-s', '--stop-cli',
        action='store_true',
        help="Stop odoo image, requires -c options.")
    parser.add_argument(
        '-r', '--run-cli',
        action='store_true',
        help="Run odoo image, requires -c option")
    parser.add_argument(
        '-c',
        action='append',
        dest='client',
        help="Client name.")
    parser.add_argument(
        '-u', '--update-all',
        action='store_true',
        help="Update all, requires -c and -m options. It stops the client,"
             "then runs an update an finally starts the client again. "
             "The update is made over the clientname_prod database")
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help="Go verbose mode. Prints every command")
    parser.add_argument(
        '-m',
        action='append',
        dest='module',
        help="Module to update or all for updating all the registered "
             "modules. ")

    args = parser.parse_args()
    client_name = get_param(args, 'client')

    if args.stop_cli:
        stop_client(client_name, args.verbose)

    if args.run_cli:
        run_client(client_name, args.verbose)

    if args.update_all:
        stop_client(client_name, args.verbose)
        module = get_param(args, 'module')
        update_all(client_name, module, args.verbose)
        run_client(client_name, args.verbose)
