#!/usr/bin/env bash

# arrancar base de datos para la version 11
sudo docker run -d \
	-e POSTGRES_USER=odoo \
	-e POSTGRES_PASSWORD=odoo \
	-v /odoo_ar/odoo-12.0e/botella/postgresql/:/var/lib/postgresql/data \
	--restart=always \
	--name pg-botella postgres:10.1-alpine


sudo docker run -d \
	--name=aeroo \
	--restart=always jobiols/aeroo-docs
